# Trello_clone Project

The application is implemented in the context of a single board.


## Features

```
Display all lists in the board
```
```
Display all cards in each list

Add a card to a list

Delete a card from a list
```
```
Add a checklist to a card

Delete a checklist from card

Display all the checklist in each card
```
```
Display all checklist items in each checklists

Add a checklists items to a checklist

Delete a checklist items from a cheklist

Update a checklist items in a checklist
```
```
Responsive:
        -Mobile
        -Tablet
        -Large Screen
```


## Tools
```
HTML
```
```
CSS
```
```
DOM
```
```
Java Script
```
```
BootStrap
```
```
Live Server: 
This is a little development server with live reload capability.

Note:
Use it for hacking your HTML/JavaScript/CSS files
but not for deploying the final site.
```

### Application Structure
```
src/index.html- The entry point to our application.
                contain Html structure
```
```
src/css- It constains the the css file for styling
```
```
src/javascript/- This file contain all the functionality of our application
```
```
src/assects- This folder contains the resources(images) for our application.
```



## Authors

* **Sumit Aswal**
