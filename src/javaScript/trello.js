//cardsWrapper is a wrapper class for all the cards
const cardsWrapper = document.querySelector('.cards-wrapper');
//cardPopUp is modal which popup when someOne click the cards
const cardPopUp = document.querySelector('.modal-content');

const boardId = '5e0597874eee0a0b05e1cc9d';
const listId = '5e059d4102a915822bfb7cc1';
const yourApiKey = '132720dfc287ee8967ec834995b748cc';
const yourApiToken =
  '3c08eee9101f611b13cf94a02f40ac18bc8ff91084444283d1a6f86b73c503be';

//cardscreate store modal for cards
let cards = '';
function cardsCreate(cardsName, cardId) {
  cards = ` <div class="cards" id=${cardId}>
  <button data-target=#${'a' + cardId} data-toggle="modal">${cardsName}</button>
  <div class="modal" id=${'a' + cardId}>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="text-dark">${cardsName}</h3>
          <button class="btn btn-secondary" data-add="add-checkList">
                      &plus;
                    </button>
          </div>
          <div class="checkListWrapper">
          </div>
        </div>
      </div>
    </div>
  <button class="delete" data-delete="delete-card">&minus;</button>
</div>`;
}

//function checkListcreate store modal for checklist
let checkList = '';
function checkListCreate(checkListName, checkListID) {
  checkList = `<div class="checkLists" id=${checkListID}>
  <h5>${checkListName}</h5>
  <div class="checksWrapper">
  </div>
  <div class="cards-button">
    <button class="btn btn-secondary" data-add="add-check-items">
      &plus; Add
    </button>
    <button class="btn btn-secondary" data-delete="delete-checklist">
      &minus; Delete All
    </button>
  </div>  
</div>
`;
}

//function checkFieldcreate store modal for checkItem
let checkField = '';
function checkFieldCreate(checkItemName, checkFieldID) {
  checkField = `  <div class=" modal-body input-group " id=${checkFieldID}>
    <div class="input-group-prepend">
      <div class="input-group-text">
      <input type="checkbox" id=${'a' + checkFieldID} name="checkbox"/>
      </div>
    </div>
    <div class="form-control">${checkItemName}</div>

     <div class="input-group-append">
    <button class="btn btn-secondary" type="submit">&minus;</button>
   </div>
  </div>
  `;
}
//createTextFeild function creates an element INPUT which is used to take input from user
function createTextField() {
  cardsName = document.createElement('input');
  cardsName.setAttribute('type', 'text');
  cardsName.setAttribute('id', 'inputid');
  cardsName.setAttribute('placeholder', 'Add New Item');
  return cardsName;
}
//after user enter the input, the input field needs to b removed, this function does that
function removeInput(input) {
  cardsWrapper.removeChild(input);
}
function removeCard(el) {
  el.remove();
}
// function to add card model to cardsWrapper
function addCard() {
  cardsWrapper.innerHTML += cards;
}
//function to add checklist model inside the respectve cards.
//checkListevent stores the element where checklist need to b append
function addCheckList(checklistEvent) {
  checklistEvent.innerHTML += checkList;
}
//adding checkList to card using the card ID
function addCheckListByID(cardId) {
  let checkListById = document.getElementById(cardId).children[1].childNodes[1]
    .childNodes[1].childNodes[3];
  checkListById.innerHTML += checkList;
}
//function to add checkfield model inside the respectve checlist.
//checItemevent stores the element where checkField need to b append
function addCheckField(checkItemEvent) {
  checkItemEvent.innerHTML += checkField;
}
//adding checkfield to checklist using the checklist ID
function addCheckFieldById(checkListID) {
  let checkFieldById = document.getElementById(checkListID).children[1];
  checkFieldById.innerHTML += checkField;
}
// function to fetch cards from Trello api
async function loadCards() {
  try {
    return await fetch(
      `https://api.trello.com/1/lists/${listId}/cards?key=${yourApiKey}&token=${yourApiToken}`
    );
  } catch (error) {
    console.log(`error ${error}`);
  }
}
// processing the json data that we fetched from trello api, to load cards in our structure
function processData(jsonData) {
  jsonData.forEach(ele => {
    cardsCreate(ele.name, ele.id);
    addCard();
  });
}
//function to fetch checklist, checkitem and status of checkFiled
async function checkListLoad(cardId) {
  try {
    const response = await fetch(
      `https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&key=${yourApiKey}&token=${yourApiToken}&checkItem_fields=name%2Cstate`
    );
    const jsonData = await response.json();
    jsonData.forEach(element => {
      checkListCreate(element.name, element.id);
      addCheckListByID(cardId);
      element.checkItems.forEach(checkField => {
        if (checkField.state === 'complete') {
          checkFieldCreate(checkField.name, checkField.id);
          addCheckFieldById(element.id);
          let check = document.createAttribute('checked');
          document
            .getElementById(`${'a' + checkField.id}`)
            .setAttributeNode(check);
        } else {
          checkFieldCreate(checkField.name, checkField.id);
          addCheckFieldById(element.id);
        }
      });
    });
  } catch (error) {
    console.log(`error ${error}`);
  }
}
//the jsonData is the object that is fetched from the trello api, and this function is calling cheklistLoad to load checklist and checkitems
function loadTrelloCheckList(jsonData) {
  jsonData.forEach(ele => {
    checkListLoad(ele.id).catch({});
  });
}
// this function add cards to trello list when ever we make a new card
async function addCardInTrello(cardName) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&keepFromSource=all&key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'POST'
      }
    );
    let data = await response.json();
    cardsCreate(cardName, data.id);
    addCard();
  } catch (error) {
    console.log(`error ${error}`);
  }
}
// this remove cards from trello list if removed from our side
async function removeCardTrello(removeCardID) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/cards/${removeCardID}?key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'delete'
      }
    );
  } catch (error) {
    console.log(`error ${error}`);
  }
}
//function to add checklist modal when ever user created one
function addCheckLists(classToappendIN) {
  let cardsName = createTextField();
  classToappendIN.insertAdjacentElement('afterend', cardsName);
}
//function to add checkItems modal when ever user created one
function addCheckInput(classToappendIN) {
  let checkInputName = createTextField();
  classToappendIN.insertAdjacentElement('beforebegin', checkInputName);
}
//function to add checklist to trello when ever user created one
async function addCheckListTrello(checkListName, cardId, checklistEvent) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/checklists?idCard=${cardId}&name=${checkListName}&key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'POST'
      }
    );
    const data = await response.json();
    checkListCreate(checkListName, data.id);
    addCheckList(checklistEvent);
  } catch (error) {
    console.log('error ' + error);
  }
}
//function to add checkItems to trello when ever user created one
async function addCheckFieldTrello(checklistId, checkItemName, event) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemName}&pos=bottom&checked=false&key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'POST'
      }
    );
    const data = await response.json();
    checkFieldCreate(checkItemName, data.id);
    addCheckField(event);
  } catch (error) {
    console.log('error ' + error);
  }
}
//to delete one checklist from trello when user delete it from his side
async function removeCheckListTrello(checkID, checklistId) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkID}?key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'delete'
      }
    );
  } catch (error) {
    console.log(`error ${error}`);
  }
}
//to delete complete checklist from trello when user delete it from his side
async function removeCompleteCheckListTrello(checkListID) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/checklists/${checkListID}?key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'delete'
      }
    );
  } catch (error) {
    console.log(`error ${error}`);
  }
}
//this function send PUT(update) request when ever user tick or unTick the checked filed in checkItem
async function updateCheckItem(cardID, checkItemId, state) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemId}?state=${state}&key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'PUT'
      }
    );
  } catch (error) {
    console.log('error ' + error);
  }
}
//event listner for all the  clicks on list container
function listClick(event) {
  // console.log(event);
  if (event.path[2].getAttribute('data-delete') === 'delete-card') {
    let cardsName = createTextField();
    cardsWrapper.appendChild(cardsName);
  }
  if (event.path[0].getAttribute('data-add') === 'add-checkList') {
    let classToappendIN = event.target.parentElement;
    addCheckLists(classToappendIN);
  }
  if (event.path[0].getAttribute('data-add') === 'add-check-items') {
    let classToappendIN = event.target.parentElement;
    addCheckInput(classToappendIN);
  }
  if (event.path[0].getAttribute('data-delete') === 'delete-checklist') {
    let checkListID = event.path[2].id;
    removeCompleteCheckListTrello(checkListID).catch({});
    removeCard(event.path[2]);
  }
  if (event.path[0].getAttribute('data-delete') === 'delete-card') {
    removeCardTrello(event.target.parentElement.id).catch({});
    removeCard(event.path[1]);
  } else if (event.target.innerHTML === '−') {
    let checkID = event.path[2].id;
    let checklistId = event.path[4].id;
    removeCheckListTrello(checkID, checklistId).catch({});
    removeCard(event.path[2]);
  }
}
//event listner for all the KeyPree(only enter) on list conatiner
function keyPress(event) {
  let key = event.which || event.keyCode;
  if (key === 13 && event.path[1].className === 'cards-wrapper') {
    let input = document.getElementById('inputid');
    addCardInTrello(input.value).catch({});

    removeCard(input);
  }
  if (key === 13 && event.path[1].className === 'modal-content') {
    let input = document.getElementById('inputid');
    addCheckListTrello(
      input.value,
      event.path[4].id,
      event.path[1].lastElementChild
    ).catch({});
    removeCard(input);
  }
  if (key === 13 && event.path[1].className === 'checkLists') {
    let input = document.getElementById('inputid');
    addCheckFieldTrello(
      event.path[1].id,
      input.value,
      event.path[1].childNodes[3]
    ).catch({});
    removeCard(input);
  }
}
//event listner for tick and untick field
function changeState(event) {
  if (event.target.checked) {
    const cardID = event.path[10].id;
    const checkItemId = event.path[3].id;
    const state = 'complete';
    updateCheckItem(cardID, checkItemId, state).catch({});
    let check = document.createAttribute('checked');
    document.getElementById(`${'a' + checkItemId}`).setAttributeNode(check);
  } else if (event.path[1].className === 'input-group-text') {
    const cardID = event.path[10].id;
    const checkItemId = event.path[3].id;
    const state = 'incomplete';
    updateCheckItem(cardID, checkItemId, state).catch({});
  }
}

function init() {
  // function to fetch cards from Trello api
  loadCards()
    .then(Response => Response.json())
    .then(data => {
      processData(data);
      loadTrelloCheckList(data);
    })
    .catch(err => console.log(err));

  //all the eventListners are called on class listd
  const lists = document.querySelector('.lists');

  //event listner for all the  clicks on list container
  lists.addEventListener('click', listClick);
  // lists.removeEventListener('click', listClick);

  //event listner for all the KeyPree(only enter) on list conatiner
  lists.addEventListener('keypress', keyPress);

  //event listner for tick and untick field
  lists.addEventListener('change', changeState);
}
init();
